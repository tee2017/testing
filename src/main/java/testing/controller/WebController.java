package testing.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import testing.model.Answer;
import testing.model.Question;
import testing.model.User;
import testing.model.UserAnswers;
import testing.repository.QuestionRepository;
import testing.repository.UserAnswersRepository;
import testing.repository.UserRepository;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by nephrite on 17.09.17.
 */
@RestController
@RequestMapping("/data")

public class WebController {
    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    UserAnswersRepository userAnswersRepository;

    @Autowired
    UserRepository userRepository;

    HashMap<String, User> mapUsers = new HashMap<String, User>();

    @RequestMapping("/saveUser")
    public void saveDataUser(@RequestParam("userName") String username,
                             @RequestParam("email") String email,
                             @RequestParam("tel") String tel,
                             HttpServletRequest request,
                             HttpServletResponse response) {
        Cookie cookie = new Cookie("cookieName", username + email);
        cookie.setMaxAge(7200);
        response.addCookie(cookie);
        User user = userRepository.save(new User(username, email, tel));
        mapUsers.put(cookie.getValue(), user);
    }

    @RequestMapping(value = "/getQuestion", method = RequestMethod.GET, produces = "application/json")
    public Question getQuestion(HttpServletResponse response,
                                @CookieValue("cookieName") Cookie cookieName) throws IOException {
        User user = mapUsers.get(cookieName.getValue());
        Question question = questionRepository.findOne(user.getCurrentQuestionId());
        if (question != null) {
            return question;

        } else response.sendRedirect("/index.html#end");
        return null;
    }

    @RequestMapping("/answerFront")
    public void answerFront(@RequestParam String answer,
                            @RequestParam int questionId,
                            @CookieValue("cookieName") Cookie cookieName) {
        User user = mapUsers.get(cookieName.getValue());
        if (questionId != user.getCurrentQuestionId()) {
            return;
        }
        userAnswersRepository.save(new UserAnswers(user, questionRepository.getOne(questionId), answer));
        user.setCurrentQuestionId(user.getCurrentQuestionId() + 1);
    }

    //нужен метод, выдающий нам все вопросы какой-либо категории
    @RequestMapping("/allIssues")
    public List<Question> getIssues(@RequestParam String category) {
        return questionRepository.findAllIssues(category);
    }

    @RequestMapping("/deleteQuestion")
    public void deleteQuestion(@RequestParam int id){
        questionRepository.delete(id);
    }


    @RequestMapping("/addQuestion")
    public void addQuestion(@RequestBody Question question){
        questionRepository.save(question);
    }


}
