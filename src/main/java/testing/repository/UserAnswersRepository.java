package testing.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import testing.model.UserAnswers;

/**
 * Created by nephrite on 17.09.17.
 */
public interface UserAnswersRepository extends JpaRepository<UserAnswers, Integer> {

}
