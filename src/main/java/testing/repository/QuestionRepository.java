package testing.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import testing.model.Question;

import java.util.List;

/**
 * Created by nephrite on 17.09.17.
 */
public interface QuestionRepository extends JpaRepository<Question,Integer> {
    @Query(value ="select q from  Question q where q.category = :category")
    List<Question> findAllIssues(@Param("category") String category);
}
