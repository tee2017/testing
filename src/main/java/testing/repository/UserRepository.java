package testing.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import testing.model.User;

/**
 * Created by nephrite on 17.09.17.
 */
public interface UserRepository extends JpaRepository <User, Integer> {

}
