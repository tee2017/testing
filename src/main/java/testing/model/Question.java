package testing.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "issues")
public class Question {


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(name = "question")
    private String question;

    @Column(name = "category")
    private String category;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "idQuestion")
    private List<Answer> answers;

    @Column(name = "type")
    private String type;

    public Question() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    @Override
    public String toString() {
        return "QuestionRepository{" +
                "id=" + id +
                ", question='" + question + '\'' +
                ", category='" + category + '\'' +
              //  ", answers=" + answers +
                ", type='" + type + '\'' +
                '}';
    }
}
