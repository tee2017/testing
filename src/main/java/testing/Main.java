package testing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@EnableAutoConfiguration
@Configuration
@ComponentScan(basePackages = {"testing.model", "testing.controller"})
public class Main {
    public static void main(String[] args) throws Exception {

        SpringApplication.run(Main.class, args);
    }
}
