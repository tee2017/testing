package testing;



import java.io.Closeable;
import java.io.IOException;
import java.sql.*;

/**
 * Created by nephrite on 16.09.17.
 */
public class DBManager implements Closeable {
    private Connection connection = null;

    public DBManager() {
        try {
            org.h2.Driver.load();
            connection = DriverManager.getConnection("jdbc:h2:~/test", "trinity", "trinity");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void close() throws IOException {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//    public QuestionRepository getQuestion(int id) throws SQLException {
//        try (PreparedStatement statement = connection.prepareStatement("select id, question from issues where id=" + id);
//        ) {
//            ResultSet resultSet = statement.executeQuery();
//            if (resultSet.next()) {
//                return new QuestionRepository(id, resultSet.getString("question"), resultSet.getString("question"));
//            } else return null;
//        } catch (SQLException e) {
//            return null;
//        }
//
//    }
//
//    public List<Answer> getAnswers(int idQuestion) {
//        List<Answer> answers = new LinkedList<>();
//        try (PreparedStatement statement = connection.prepareStatement("select id, answer, type,idquestion,iscorrect from answers where idquestion=" + idQuestion);
//        ) {
//            ResultSet resultSet = statement.executeQuery();
//            while (resultSet.next()) {
//                answers.add(new Answer(resultSet.getInt("id"), resultSet.getString("answer"), resultSet.getString("type"), resultSet.getInt(idQuestion), resultSet.getBoolean("iscorrect")));
//
//            } return answers;
//        } catch (SQLException e) {
//            return null;
//        }
//
//
//
//    }
   // public
}
