$(function () {

    $(document).ready(function () {
        loadPage(); // загрузка main
        window.onhashchange = function () {
            loadContent();

        }
    })
});

// Загрузка контента

function loadContent() {
  
    var $mainContent = $('#mainContent');
    if ($.isEmptyObject($.Parser())) {
        
        loadTemplate('templates/auth/auth', function(template){
               dust.renderSource(template, {}, function (err, out) {
                    $('#mainContent').append(out);
                    $.ajax({
                        url: 'templates/auth/auth.js'
                    }).done(function(){
                       window['auth']();
                    });
                });
        })
        
        /*
        $.ajax({
            url: "templates/auth/auth.dust",
            type: "GET"
        }).done(function (template) {
            var data = {}
            dust.renderSource(template, data, function (err, out) {
                $mainContent.append(out);
                //первая запись в хэш
                //location.href = "#main";

            });
        });
       */ 

    } else {
        var path = $.Parser().path[0];
        loadTemplate('templates/' + path + '/' + path, function (template) {
            loadScript('templates/' + path + '/' + path, template);
        })
    }
}


// Загрузка main (header и footer)

function loadPage() {
    //location.href = "";
    var $wrapper = $('#wrapper');
    $.ajax({
        url: "templates/main.dust",
        type: "GET"
    }).done(function (template) {
        var data = {}
        dust.renderSource(template, data, function (err, out) {
            $wrapper.append(out);
            loadContent();
        });
    });
}


// Загрузка шаблона

function loadTemplate(path, callback) {
    console.log("загружаем  temp path " + path);
    $.ajax({
        url: path + ".dust"
    }).done(function (template) {
        
        callback(template);
    })
}


// Загрузка скрипта

function loadScript(path, template) {
    console.log("загружаем script path " + path);
    $.ajax({
        url: path + ".js"
    }).done(function () {
        window[$.Parser().path[0]](template);
    })
}


//парселка
(function ($) {
    $.Parser = function (url) {
        var objectParser = {};
        var string;
        var afterSharp;

        var path = location.href; //путь
        var arguments;
        var args = {};
        var url = path;

        if (url.indexOf("#") === -1) {
            return objectParser;
        } else {
            string = url.split('#');
            afterSharp = string[1];
            
            if (url.indexOf("?") === -1) {
                path = afterSharp.split("/");
                 objectParser.path = path;
                 objectParser.args = args;
            } else {
            path = afterSharp.split("?")[0].split("/");
                arguments = afterSharp.split("?")[1].split("&");
                //console.log(arguments)
                for (var i = 0; i < arguments.length; i++) {
                    var key = arguments[i].split("=")[0];
                    var value = arguments[i].split("=")[1];
                    args[key] = value;
                }

            }
            

            objectParser.path = path;
            objectParser.args = args;

        } 


        return objectParser;
    }




})(jQuery);
