function auth() {
    console.log('auth.js загрузился');

    console.log($('#inputEmail'));

    //функция проверки форм
    function checkData() {
        $inputName = $('#inputName');
        $inputEmail = $('#inputEmail');
        $inputPhone = $('#inputPhone');

        var f = true;

        if ($inputName.val() == '') {
            $inputName.addClass('is-invalid');
             f = false;
        } else {
            $inputName.removeClass('is-invalid')
            $inputName.addClass('is-valid');
           
        }

        var emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+$/;

        if (($inputEmail.val() == '') || !(emailReg.test($inputEmail.val()))) {
            $inputEmail.addClass('is-invalid');
            f = false;
        } else {
            $inputEmail.removeClass('is-invalid')
            $inputEmail.addClass('is-valid');
            
        }

        var numbReg = /^\d+$/;

        if (($inputPhone.val() == '') || !(numbReg.test($inputPhone.val()))) {
            $inputPhone.addClass('is-invalid');
             f = false;
        } else {
            $inputPhone.removeClass('is-invalid')
            $inputPhone.addClass('is-valid');
           
        }
        return f;
    }


    //переход на тест
    $buttonGoTest = $('#main-button-gotest');

    $buttonGoTest.on('click', function () {
       if (checkData()) {
         console.log('меняем хэш на test');
         goTest();
       }


        
    });

    function goTest() {
        location.href = '#test';
    }

    ;


}
